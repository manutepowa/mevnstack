import { Schema, model } from 'mongoose';
import bcrypt from 'bcrypt';

const userSchema = new Schema({
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true
    },
    name: String,
},{
    versionKey: false,
    timestamps: true
});

/**
 * USER PASSWORD VALIDATION
 */
userSchema.statics.encryptPassword = async (password) => {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
}

userSchema.statics.comparePassword = async (password, userFoundPassword) => {

    // const salt = await bcrypt.genSalt(10);
    // const hashPassword = await bcrypt.hash(password, salt);
    // console.log(hashPassword, userFoundPassword);
    return await bcrypt.compare(password, userFoundPassword);
}

export default model('User', userSchema);
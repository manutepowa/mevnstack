import axios from "axios";
import fs from 'fs'
// GET ALL POSTS
export const getPosts = async (req, res) => {
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvZmVtZWN2LnBsYXlvZmZpbmZvcm1hdGljYS5jb20iLCJhdWQiOiJodHRwczpcL1wvZmVtZWN2LnBsYXlvZmZpbmZvcm1hdGljYS5jb20iLCJpZFVzdWFyaSI6IjI3MiIsIm5vbVVzdWFyaSI6IkZlZGVyYWNpJUYzK0VzcG9ydHMrZGUrTXVudGFueWEraStFc2NhbGFkYStkZStsYStDb211bml0YXQrVmFsZW5jaWFuYSIsInJvbCI6IlJPTF9VRkVERVJBQ0lPIiwiaWRTaW1wYXRpdHphbnQiOiIxIiwiaWRGZWRlcmF0IjpudWxsLCJpZEFyYml0cmUiOm51bGwsInBlcm1pc3Npb25zIjoxNX0.nw3VgbtD_3-ceCGW6Rs8N2BTWaMdCgIw8TvWfSy4QhM'
    }
    const path = 'https://femecv.playoffinformatica.com/api.php/api/v1.0/clubs';

    const clubs = await axios.get(path, {
        headers,
        params: {
            limit: 705
        }
    })

    const clubesActivos = clubs.data.filter((club) => club.estatClub === "CLUBALT")
    fs.writeFileSync('clubes_activos.json', JSON.stringify(clubesActivos));
    res.json(clubesActivos);
};

export const readClubes = async (req, res) => {

    // const clubesActivos = clubs.data.filter((club) => club.estatClub === "CLUBALT")
    // fs.writeFileSync('clubes_activos.json', JSON.stringify(clubesActivos));
    const rawClubes = fs.readFileSync('clubes_activos.json');
    const clubes = JSON.parse(rawClubes);
    res.json(clubes);
};

// GET POST BY ID
export const getPost = async (req, res) => {
    let posts = {};
    const ee = "hola"
    try {
        posts = await axios.get(
            "https://jsonplaceholder.typicode.com/posts/" + req.params.id
        );

        const data = posts.data;

    } catch (error) {
        return res.json({
            status: error.response.status,
            message: "Resource not found",
        });
    }

    res.json(posts.data);
};

// GET ONE POST BY ID
export const updatePost = async (req, res) => {
    console.log(req.body);

    const posts = await axios.patch("https://jsonplaceholder.typicode.com/posts/" + req.params.id,
        req.body,
        {
            headers: {
                "Content-Type": "application/json",
            },
        }
    );

    // console.log(posts.data);
    // console.log(posts.data)
    res.status(200).json(posts.data);
};

import User from '../models/user.model';
import { SECRET_JWT } from "../config";
import jwt from 'jsonwebtoken';



export const getUsers = async (req, res) => {
    const users = await User.find({}, {password: 0});

    if (!users) return res.status(404).json({message: "Not users found"})

    res.json(users)
}

export const getUser = async (req, res) => {
    try{
        const user = await User.findById(req.params.id)

        //User null
        if (!user) return res.status(404).json({message: "User not found"})
        res.json(user)
    }catch (e) {
        res.status(400).json({message: "Bad request"})
    }
}


/**
 * AUTH USER
 *
 */

export const registerUser = async (req, res) => {
    const { email, password, name } = req.body;

    const user = new User({
        email,
        password: await User.encryptPassword(password),
        name,
    });

    try{
        let newUser = await user.save();
        const token = await jwt.sign({ id: newUser._id }, SECRET_JWT, {
            expiresIn: 86400 // 24 horas
        });

        delete newUser.password;
        res.json({
            error: "",
            user: newUser,
            token
        });
    }catch(err) {
        res.status(400).json({
            error,
            user: "",
            token: ""
        });
    }
}


export const loginUser = async (req, res) => {
    const { email, password } = req.body;

    try {
        const user = await User.findOne({email});

        //Check if user exists
        if (!user) return res.status(404).json({ message: "User not found" });

        //Compare password
        const matchPassword = await User.comparePassword(password, user.password);
        if (!matchPassword) return res.status(404).json({ message: "Bad Password !!" });

        const token = await jwt.sign({ id: user._id }, SECRET_JWT, {
            expiresIn: 86400 // 24 horas
        });

        delete user.password;
        res.json({
            error: "",
            user: user,
            token
        });
    } catch (error) {
        res.status(400).json({
            error,
            user: "",
            token: ""
        });
    }
}

// Routes
// router.get('/users', async (req, res) => {
//     const data = await axios.get('https://jsonplaceholder.typicode.com/posts');
//     res.json(data.data)
// })

// // Routes
// router.get('/users/:id', async (req, res) => {
//     const { id } = req.params;
//     const data = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
//     res.json(data.data)
// })
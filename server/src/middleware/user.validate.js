import Joi from 'joi';
import User from '../models/user.model';
import { SECRET_JWT } from "../config";
import jwt from 'jsonwebtoken';

const userSchema = Joi.object({
    email: Joi.string().required().email(),
    password: Joi.string().min(6).required()
});


export const userValidate = (req, res, next) => {
    const { email, password } = req.body;

    const { error } = userSchema.validate({ email, password });

    if(error){
        return res.status(400).json({message: error.details[0].message})
    }

    next();
}

export const userExists = async (req, res, next) => {
    const { email } = req.body;

    const user = await User.findOne({email});

    if(user){
        return res.status(400).json({message: "User already exists"})
    }

    next();
}


export const verifyToken = async (req, res, next) => {
    const token = req.headers['x-access-token'];

    if(!token) return res.json({ message: "No token provided" });

    try {
        const decoded = await jwt.verify(token, SECRET_JWT);

        const user = await User.findById(decoded.id, {password: 0});

        next();
    } catch (error) {
        return res.status(400).json({ message: "Token not valid" });
    }
}
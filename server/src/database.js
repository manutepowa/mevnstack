import mongoose from 'mongoose';
import { MONGODB_URI } from "./config";

mongoose
    .connect(MONGODB_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
    })
    .then((db) => console.log("DB is connected"))
    .catch((error) => console.error(error));
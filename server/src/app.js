import express from 'express';
import morgan from 'morgan';

// Routes
import UserRouter from './routes/user.router';
import PlaceholderRouter from './routes/placeholder.router';


const app = express();

// Settings
app.set('port', process.env.PORT || 3001);

// Middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({extended: true})); //Explicar para que sirve
app.use(express.json()); //Explicar para que sirve

// Routes
app.use('/user', UserRouter);
app.use('/placeholder', PlaceholderRouter);

export default app;
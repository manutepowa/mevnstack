import { Router } from 'express';
import * as userCtrl from '../controllers/user.controller';
import { userValidate, userExists, verifyToken } from '../middleware/user.validate';


const router = Router();


router.get('/list', verifyToken, userCtrl.getUsers);
router.get('/list/:id', userCtrl.getUser);


router.post('/register', [ userValidate, userExists ], userCtrl.registerUser);
router.post('/login', [ userValidate ], userCtrl.loginUser);



export default router;
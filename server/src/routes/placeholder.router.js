import { Router } from 'express'
import * as PlaceHolder from '../controllers/placeholder.controller'
const router = Router();


router.get('/posts', PlaceHolder.getPosts);
router.get('/readclubes', PlaceHolder.readClubes);
router.get('/posts/:id', PlaceHolder.getPost);
router.patch('/posts/:id', PlaceHolder.updatePost);


export default router;